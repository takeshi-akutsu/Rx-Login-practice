//
//  LoginViewModel.swift
//  Rx-Login-practice
//
//  Created by Takeshi Akutsu on 2018/08/10.
//  Copyright © 2018年 Takeshi Akutsu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct LoginViewModel {
    var email = Variable<String>("")
    var password = Variable<String>("")
    
    var isValid: Observable<Bool> {
        return Observable.combineLatest(email.asObservable(), password.asObservable()) { email, password in
            email.characters.count >= 3 && password.characters.count >= 3
        }
    }
}
