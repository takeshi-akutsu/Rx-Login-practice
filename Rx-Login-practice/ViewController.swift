//
//  ViewController.swift
//  Rx-Login-practice
//
//  Created by Takeshi Akutsu on 2018/08/10.
//  Copyright © 2018年 Takeshi Akutsu. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ViewController: UIViewController {
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var stateLabel: UILabel!
    
    var loginVM = LoginViewModel()
    let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _ = emailTextField.rx.text.map { $0 ?? "" }.bind(to: loginVM.email)
        _ = passwordTextField.rx.text.map { $0 ?? "" }.bind(to: loginVM.password)
        
        loginVM.isValid.subscribe(onNext: { [weak self] (isValid) in
            self?.registerButton.isEnabled = isValid
            
            self?.stateLabel.text = isValid ? "enabled" : "not enabled"
            self?.stateLabel.textColor = isValid ? .green : .red
            
        }).disposed(by: bag)
        
    }

}

